import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:notifyme/add_friend.dart';
import 'package:notifyme/friend.dart';
import 'package:notifyme/group.dart';
import 'package:notifyme/setting.dart';
import 'package:notifyme/timeline.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:cron/cron.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {

  final String currentUserId;

  HomePage({Key key, @required this.currentUserId}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState(currentUserId: currentUserId);
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  _HomePageState({Key key, @required this.currentUserId});

  final String currentUserId;
  TabController _controller;

  final dateFormat = new DateFormat('dd-MM-yyyy');
  final timeFormat = new DateFormat('HH');
  DateTime _date = new DateTime.now();
  TimeOfDay _time;


  @override
  void initState() {
    super.initState();
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
    _controller = new TabController(length: 3, vsync: this);

    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);

    var cron = new Cron();
    cron.schedule(new Schedule.parse('*/1 * * * *'), () async {
      _time = TimeOfDay.now();
      debugPrint(timeNow());
        QuerySnapshot querySnapshot = await Firestore.instance.collection('notification').where('adminId', isEqualTo: currentUserId).getDocuments();
        var list = querySnapshot.documents;
        for (int i = 0; i <= list.length; i++){
          if (list != 0){
              if (list[i]['notificationScheduling'] == 'Scheduled'){
              if (list[i]['date'] == dateFormat.format(_date)){
                if (list[i]['time'] == timeNow()){
                  try {
                    final result = await InternetAddress.lookup('google.com');
                    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                      debugPrint(list[i]['title']);
                      Response response;
                      var dio = Dio();
                      dio.options.baseUrl = "http://skripsinotifyme.000webhostapp.com";
                      FormData formData = new FormData.fromMap({"idgroup": list[i]['groupId'], "title": list[i]['groupName']+": "+list[i]['title'], "message": list[i]['message']});

                      try{
                        response = await dio.post("/sendMultiplePush.php",
                            data: formData, options: Options(method: "POST"));
                        debugPrint("response dio: " + response.data.toString());


                        debugPrint('yeay got it: '+list[i]['title']);
                        debugPrint(list[i]['notificationId']);

                        final collRef = Firestore.instance.collection('notification');
                        DocumentReference docReferance = collRef.document(list[i]['notificationId']);
                        docReferance.updateData({'status': 'Sent', 'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),});
                      } catch(e){
                        debugPrint('fail to push notification');
                        final collRef = Firestore.instance.collection('notification');
                        DocumentReference docReferance = collRef.document(list[i]['notificationId']);
                        docReferance.updateData({'status': 'Fail', 'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),});

                        var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
                            'your channel id', 'your channel name', 'your channel description',
                            importance: Importance.Max, priority: Priority.High);
                        var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
                        var platformChannelSpecifics = new NotificationDetails(
                            androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
                        await flutterLocalNotificationsPlugin.show(
                          1,
                          'Push Notification Fail: '+list[i]['title']+" for "+list[i]['groupName'],
                          'Push Notification Status: Fail',
                          platformChannelSpecifics,
                          payload: 'Default_Sound',
                        );
                      }

                    }
                  } on SocketException catch (_) {
                    debugPrint('fail to push notification');
                    final collRef = Firestore.instance.collection('notification');
                    DocumentReference docReferance = collRef.document(list[i]['notificationId']);
                    docReferance.updateData({'status': 'Fail', 'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),});

                    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
                        'your channel id', 'your channel name', 'your channel description',
                        importance: Importance.Max, priority: Priority.High);
                    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
                    var platformChannelSpecifics = new NotificationDetails(
                        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
                    await flutterLocalNotificationsPlugin.show(
                      1,
                      'Push Notification Fail: '+list[i]['title']+" for "+list[i]['groupName'],
                      'Push Notification Status: Fail',
                      platformChannelSpecifics,
                      payload: 'Default_Sound',
                    );

                  }
                }
              }
            }
          }
        }
        debugPrint('every one minute');
    });
  }

  String timeNow(){
    return _time.format(context);
  }

  Future onSelectNotification(String payload) async {

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
//      key: scaffoldKey,
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.redAccent,
        iconTheme: new IconThemeData(color: const Color(0xFFFFFFFF),),
        title: new Text("Notify Me",
          style: new TextStyle(
            color:
            const Color(0xFFFFFFFF),
            fontSize: 20.0,
            fontWeight: FontWeight.w300,
            letterSpacing: 0.3,
          ),
        ),

        bottom: new TabBar(
          controller: _controller,
          indicatorSize: TabBarIndicatorSize.tab,
          labelColor: Color(0xFFFFFFFF),
          tabs: const <Tab>[
            const Tab(text: 'Timeline'),
            const Tab(text: 'Group'),
            const Tab(text: 'Profile'),
          ],
        ),
      ),
      body: new TabBarView(
        controller: _controller,
        children: <Widget>[
          new TimelineScreen(currentUserId: currentUserId,),
          new GroupDashboard(currentUserId: currentUserId,),
          new Settings(),
        ],
      ),
    );

  }
}