class GroupModel{
  String _name;
  String _photoUrl;
  String _id;
  List<String> _member;

  String get get_name => _name;
  String get get_photoUrl => _photoUrl;
  String get get_id => _id;
  List<String> get get_member => _member;

  void set setName(String name){
    _name = name;
  }
  void set setPhotoUrl(String photoUrl){
    _photoUrl = photoUrl;
  }
  void set setId(String id){
    _id = id;
  }
  void set setMember(List<String> member){
    _member = member;
  }
}