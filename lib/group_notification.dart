import 'package:notifyme/add_notification.dart';
import 'package:notifyme/group_notification_attendance.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'const.dart';

class GroupNotification extends StatefulWidget {
  final String groupId;
  final String currentUserId;
  final String groupName;
  final List<String> memberGroup;

  GroupNotification({Key key, this.groupId, this.currentUserId, this.memberGroup, this.groupName})
      : super(key: key);

  @override
  _GroupNotificationState createState() =>
      _GroupNotificationState(groupId: groupId, currentUserId: currentUserId, memberGroup: memberGroup, groupName: groupName);
}

class _GroupNotificationState extends State<GroupNotification> {
  final String groupId;
  final String currentUserId;
  final String groupName;
  final List<String> memberGroup;

  _GroupNotificationState({@required this.groupId, @required this.currentUserId, @required this.memberGroup, @required this.groupName});

  bool isLoading = false;

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
//    GroupModel groupModel = new GroupModel();
//    groupModel.setId = document['idGroup'];
//    groupModel.setName = document['nameGroup'];
//    groupModel.setPhotoUrl = document['photoUrl'];
//    List<String> members = List.from(document['members']);
//    dataList.add(groupModel);

    return Container(
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Card(
          shape:
              BeveledRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
          elevation: 6.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Center(
                child: ListTile(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: new Text(document['title']),
                            content: new Text(document['message']),
                            actions: <Widget>[
                              new FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: new Text('Close'))
                            ],
                          );
                        });
                  },
                  leading: CircleAvatar(
                    child: Text(
                      document['title'][0].toString().toUpperCase(),
                      style: TextStyle(
                          fontSize: 24.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                  title: Text(
                    document['title'].toString(),
                    style: TextStyle(),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Push in " +
                        document['date'] +
                        " at " +
                        document['time'] +
                        " o'clock"),
                  ),
                  trailing: IconButton(
                    color: Colors.redAccent,
                    icon: (document['status'] == 'Waiting')
                        ? Icon(Icons.timer)
                        : (document['status'] == 'Fail') ? Icon(Icons.close) : Icon(Icons.check),
                    onPressed: () {},
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Nofitication"),
        backgroundColor: Colors.redAccent,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.assignment_ind),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => GroupNotificationAttendance(groupId: groupId, currentUserId: currentUserId, memberGroup: memberGroup, groupName: groupName,)));
              }
          )
        ],
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Container(
              child: StreamBuilder(
                stream:
                    Firestore.instance.collection('notification').where('groupId', isEqualTo: groupId).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) =>
                          buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                    );
                  }
                },
              ),
            ),
            Positioned(
              child: isLoading
                  ? Container(
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                        ),
                      ),
                      color: Colors.white.withOpacity(0.8),
                    )
                  : Container(),
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        backgroundColor: Colors.redAccent,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddNotification(
                        idGroup: groupId,
                        currentUserId: currentUserId,
                        memberGroup: memberGroup,
                        groupName: groupName,
                      )));
        },
        child: Icon(Icons.add_alert),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(16.0))),
      ),
    );
  }
}
